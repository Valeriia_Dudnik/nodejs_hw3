const { Load } = require('../models/Load.js');
const { Truck } = require('../models/Truck.js');
const {
  states, loadStatus, DRIVER, SHIPPER,
} = require('../tools/const.js');
const { chooseTruckType } = require('../tools/chooseTruckType.js');

const getLoads = async ({
  userId, status, role, limit, offset,
}) => {
  if (role === DRIVER) {
    const loads = await Load.find({
      assigned_to: userId,
      status: [loadStatus.ASSIGNED, loadStatus.SHIPPED],
    })
      .skip(offset)
      .limit(limit);
    return loads;
  }

  if (role === SHIPPER) {
    const loads = await Load.find({ created_by: userId })
      .skip(offset)
      .limit(limit);
    return loads;
  }
};

const addLoad = async ({ userId, payload }) => {
  await Load.create({
    created_by: userId,
    name: payload.name,
    payload: payload.payload,
    pickup_address: payload.pickup_address,
    delivery_address: payload.delivery_address,
    dimensions: payload.dimensions,
  });
};

const getActiveLoad = async (userId) => {
  const load = await Load.findOne({
    assigned_to: userId,
    status: loadStatus.ASSIGNED,
  });

  return load;
};

const iterateState = async (userId) => {
  const load = await Load.findOne({
    assigned_to: userId,
    status: loadStatus.ASSIGNED,
  });

  if (!load) {
    return { message: 'Load not found' };
  }

  const oldState = states.indexOf(load.state);
  const newState = states[oldState + 1];

  if (newState === states[3]) {
    load.state = newState;
    load.status = loadStatus.SHIPPED;
    await load.save();
    return { newState };
  }

  load.state = newState;
  await load.save();

  return { newState };
};

const getLoadById = async (userId, loadId) => {
  const load = await Load.findOne(
    { _id: loadId, created_by: userId },
  );
  return load;
};

const getLoadStatusById = async (userId, loadId) => {
  const load = await Load.findOne(
    { _id: loadId, created_by: userId },
  );
  return load.status;
};

const updateLoadById = async (userId, loadId, payload) => {
  const load = await Load.findOneAndUpdate(
    { _id: loadId, created_by: userId, status: 'NEW' },
    payload,
  );
  return load;
};

const deleteLoadById = async (userId, loadId) => {
  const load = await Load.findOneAndDelete(
    { _id: loadId, created_by: userId },
  );
  return load;
};

const postLoadById = async (userId, loadId) => {
  const load = await Load.findOne(
    { _id: loadId, created_by: userId },
  );
  const truckType = chooseTruckType({
    payload: load.payload,
    dimensions: load.dimensions,
  });
  const truck = await Truck.findOne({
    status: 'IS',
    assigned_to: { $exists: true },
    type: truckType,
  });
  if (!truck) return false;

  truck.status = 'OL';
  await truck.save();

  load.status = loadStatus.ASSIGNED;
  load.assigned_to = truck.assigned_to;
  load.state = states[0];
  load.logs.push({
    message: `Load was assigned to driver with id ${truck.assigned_to}`,
  });
  await load.save();
  return true;
};

const getLoadShippingInfoById = async (userId, loadId) => {
  const load = await Load.findOne(
    { _id: loadId, created_by: userId },
  );
  if (load.status !== loadStatus.ASSIGNED) {
    return { message: 'Load has not been assigned' };
  }
  const truck = await Truck.findOne(
    { assigned_to: load.assigned_to },
  );
  return { load, truck };
};

module.exports = {
  getLoads,
  addLoad,
  getActiveLoad,
  iterateState,
  getLoadById,
  getLoadStatusById,
  updateLoadById,
  deleteLoadById,
  postLoadById,
  getLoadShippingInfoById,
};
