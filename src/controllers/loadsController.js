const {
  getLoads,
  addLoad,
  getActiveLoad,
  iterateState,
  getLoadById,
  getLoadStatusById,
  updateLoadById,
  deleteLoadById,
  postLoadById,
  getLoadShippingInfoById,
} = require('../services/loadsService.js');

const getUserLoads = async (req, res) => {
  const { status, limit = 10, offset = 0 } = req.params;
  const { role, userId } = req.user;
  const allLoads = await getLoads({
    userId,
    status,
    role,
    limit,
    offset,
  });
  return res.status(200).send({ loads: allLoads });
};

const addUserLoad = async (req, res) => {
  const { userId } = req.user;

  await addLoad({ userId, payload: req.body });
  return res.status(200).send({ message: 'Load created successfully' });
};

const getUserActiveLoad = async (req, res) => {
  const { userId } = req.user;
  const activeLoad = await getActiveLoad(userId);
  if (!activeLoad) {
    return res.status(400).send({ message: 'Load not found' });
  }
  return res.status(200).send({ load: activeLoad });
};

const iterateToNextLoadState = async (req, res) => {
  const { userId } = req.user;
  const { newState, message } = await iterateState(userId);
  if (message) {
    return res.status(400).send({ message });
  }
  return res.status(200).send({ message: `Load state changed to ${newState}` });
};

const getUserLoadById = async (req, res) => {
  const { userId } = req.user;
  const loadId = req.params.id;
  const load = await getLoadById(userId, loadId);

  if (!load) {
    return res.status(400).send({ message: 'Load not found' });
  }
  return res.status(200).send({ load });
};

const updateUserLoadById = async (req, res) => {
  const { userId } = req.user;
  const loadId = req.params.id;
  const payload = req.body;

  const status = await getLoadStatusById(userId, loadId);

  if (status !== 'NEW') {
    return res.status(400).send({ message: 'Only new loads can be updated' });
  }

  const load = await updateLoadById(userId, loadId, payload);

  if (!load) {
    return res.status(400).send({ message: 'Load not found' });
  }
  return res.status(200).send({ message: 'Load details changed successfully' });
};

const deleteUserLoadById = async (req, res) => {
  const { userId } = req.user;
  const loadId = req.params.id;
  const load = await deleteLoadById(userId, loadId);
  if (!load) {
    return res.status(400).send({ message: 'Load not found' });
  }
  return res.status(200).send({ message: 'Load deleted successfully' });
};

const postUserLoadById = async (req, res) => {
  const { userId } = req.user;
  const loadId = req.params.id;
  const driver = await postLoadById(userId, loadId);
  return res.status(200).send({ message: 'Load posted successfully', driver_found: driver });
};

const getUserLoadShippingInfoById = async (req, res) => {
  const { userId } = req.user;
  const loadId = req.params.id;

  const { load, truck, message } = await getLoadShippingInfoById(userId, loadId);
  if (message) {
    return res.status(400).send({ message });
  }
  return res.status(200).send({ load, truck });
};

module.exports = {
  getUserLoads,
  addUserLoad,
  getUserActiveLoad,
  iterateToNextLoadState,
  getUserLoadById,
  updateUserLoadById,
  deleteUserLoadById,
  postUserLoadById,
  getUserLoadShippingInfoById,
};
