const mongoose = require('mongoose');

const User = mongoose.model('User', {
  email: {
    type: String,
    required: true,
    unique: true,
  },
  username: {
    type: String,
    required: false,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
    enum: ['SHIPPER', 'DRIVER'],
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },

});

module.exports = {
  User,
};
