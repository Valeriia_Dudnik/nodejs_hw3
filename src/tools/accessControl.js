const AccessControl = require('accesscontrol');

const access = new AccessControl();

const roles = (function () {
  access.grant('DRIVER')
    .createOwn('trucks')
    .readOwn('trucks')
    .updateOwn('trucks')
    .deleteOwn('trucks')
    .readOwn('activeLoad')
    .updateOwn('activeLoad');

  access.grant('SHIPPER')
    .createOwn('loads')
    .readOwn('loads')
    .updateOwn('loads')
    .deleteOwn('loads');

  return access;
}());

module.exports.allowAccess = function(action, resource) {
  return async (req, res, next) => {
    try {
      const permission = roles.can(req.user.role)[action](resource);
      if (!permission.granted) {
        return res.status(403).json({
          error: 'You don\'t have enough permission to perform this action',
        });
      }
      next();
    } catch (error) {
      next(error);
    }
  };
};
