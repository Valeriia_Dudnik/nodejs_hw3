const DRIVER = 'DRIVER';

const SHIPPER = 'SHIPPER';

const loadStatus = {
  NEW: 'NEW',
  POSTED: 'POSTED',
  ASSIGNED: 'ASSIGNED',
  SHIPPED: 'SHIPPED',
};

const states = [
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route to delivery',
  'Arrived to delivery',
];

const TRUCK_TYPES = {
  SPRINTER: {
    width: 300,
    length: 250,
    height: 170,
    payload: 1700,
  },
  SMALL_STRAIGHT: {
    width: 500,
    length: 250,
    height: 170,
    payload: 2500,
  },
  LARGE_STRAIGHT: {
    width: 700,
    length: 350,
    height: 200,
    payload: 4000,
  },
};

module.exports = {
  DRIVER,
  SHIPPER,
  loadStatus,
  states,
  TRUCK_TYPES,
};
