const express = require('express');

const router = express.Router();
const { getProfileInfo, deleteProfile, changeProfilePassword } = require('../controllers/usersController.js');
const { authMiddleware } = require('../middleware/authMiddleware.js');

router
  .route('/me')
  .get(authMiddleware, getProfileInfo)
  .delete(authMiddleware, deleteProfile);

router
  .route('/me/password')
  .patch(authMiddleware, changeProfilePassword);

module.exports = {
  usersRouter: router,
};
